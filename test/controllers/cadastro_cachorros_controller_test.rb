require 'test_helper'

class CadastroCachorrosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cadastro_cachorro = cadastro_cachorros(:one)
  end

  test "should get index" do
    get cadastro_cachorros_url
    assert_response :success
  end

  test "should get new" do
    get new_cadastro_cachorro_url
    assert_response :success
  end

  test "should create cadastro_cachorro" do
    assert_difference('CadastroCachorro.count') do
      post cadastro_cachorros_url, params: { cadastro_cachorro: { idade: @cadastro_cachorro.idade, nome: @cadastro_cachorro.nome, raca: @cadastro_cachorro.raca, sexo: @cadastro_cachorro.sexo } }
    end

    assert_redirected_to cadastro_cachorro_url(CadastroCachorro.last)
  end

  test "should show cadastro_cachorro" do
    get cadastro_cachorro_url(@cadastro_cachorro)
    assert_response :success
  end

  test "should get edit" do
    get edit_cadastro_cachorro_url(@cadastro_cachorro)
    assert_response :success
  end

  test "should update cadastro_cachorro" do
    patch cadastro_cachorro_url(@cadastro_cachorro), params: { cadastro_cachorro: { idade: @cadastro_cachorro.idade, nome: @cadastro_cachorro.nome, raca: @cadastro_cachorro.raca, sexo: @cadastro_cachorro.sexo } }
    assert_redirected_to cadastro_cachorro_url(@cadastro_cachorro)
  end

  test "should destroy cadastro_cachorro" do
    assert_difference('CadastroCachorro.count', -1) do
      delete cadastro_cachorro_url(@cadastro_cachorro)
    end

    assert_redirected_to cadastro_cachorros_url
  end
end
