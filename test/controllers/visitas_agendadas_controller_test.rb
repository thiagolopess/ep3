require 'test_helper'

class VisitasAgendadasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @visitas_agendada = visitas_agendadas(:one)
  end

  test "should get index" do
    get visitas_agendadas_url
    assert_response :success
  end

  test "should get new" do
    get new_visitas_agendada_url
    assert_response :success
  end

  test "should create visitas_agendada" do
    assert_difference('VisitasAgendada.count') do
      post visitas_agendadas_url, params: { visitas_agendada: { data: @visitas_agendada.data, hora: @visitas_agendada.hora } }
    end

    assert_redirected_to visitas_agendada_url(VisitasAgendada.last)
  end

  test "should show visitas_agendada" do
    get visitas_agendada_url(@visitas_agendada)
    assert_response :success
  end

  test "should get edit" do
    get edit_visitas_agendada_url(@visitas_agendada)
    assert_response :success
  end

  test "should update visitas_agendada" do
    patch visitas_agendada_url(@visitas_agendada), params: { visitas_agendada: { data: @visitas_agendada.data, hora: @visitas_agendada.hora } }
    assert_redirected_to visitas_agendada_url(@visitas_agendada)
  end

  test "should destroy visitas_agendada" do
    assert_difference('VisitasAgendada.count', -1) do
      delete visitas_agendada_url(@visitas_agendada)
    end

    assert_redirected_to visitas_agendadas_url
  end
end
