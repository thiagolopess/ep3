require "application_system_test_case"

class VisitasAgendadasTest < ApplicationSystemTestCase
  setup do
    @visitas_agendada = visitas_agendadas(:one)
  end

  test "visiting the index" do
    visit visitas_agendadas_url
    assert_selector "h1", text: "Visitas Agendadas"
  end

  test "creating a Visitas agendada" do
    visit visitas_agendadas_url
    click_on "New Visitas Agendada"

    fill_in "Data", with: @visitas_agendada.data
    fill_in "Hora", with: @visitas_agendada.hora
    click_on "Create Visitas agendada"

    assert_text "Visitas agendada was successfully created"
    click_on "Back"
  end

  test "updating a Visitas agendada" do
    visit visitas_agendadas_url
    click_on "Edit", match: :first

    fill_in "Data", with: @visitas_agendada.data
    fill_in "Hora", with: @visitas_agendada.hora
    click_on "Update Visitas agendada"

    assert_text "Visitas agendada was successfully updated"
    click_on "Back"
  end

  test "destroying a Visitas agendada" do
    visit visitas_agendadas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Visitas agendada was successfully destroyed"
  end
end
