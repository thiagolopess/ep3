require "application_system_test_case"

class CadastroCachorrosTest < ApplicationSystemTestCase
  setup do
    @cadastro_cachorro = cadastro_cachorros(:one)
  end

  test "visiting the index" do
    visit cadastro_cachorros_url
    assert_selector "h1", text: "Cadastro Cachorros"
  end

  test "creating a Cadastro cachorro" do
    visit cadastro_cachorros_url
    click_on "New Cadastro Cachorro"

    fill_in "Idade", with: @cadastro_cachorro.idade
    fill_in "Nome", with: @cadastro_cachorro.nome
    fill_in "Raca", with: @cadastro_cachorro.raca
    fill_in "Sexo", with: @cadastro_cachorro.sexo
    click_on "Create Cadastro cachorro"

    assert_text "Cadastro cachorro was successfully created"
    click_on "Back"
  end

  test "updating a Cadastro cachorro" do
    visit cadastro_cachorros_url
    click_on "Edit", match: :first

    fill_in "Idade", with: @cadastro_cachorro.idade
    fill_in "Nome", with: @cadastro_cachorro.nome
    fill_in "Raca", with: @cadastro_cachorro.raca
    fill_in "Sexo", with: @cadastro_cachorro.sexo
    click_on "Update Cadastro cachorro"

    assert_text "Cadastro cachorro was successfully updated"
    click_on "Back"
  end

  test "destroying a Cadastro cachorro" do
    visit cadastro_cachorros_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cadastro cachorro was successfully destroyed"
  end
end
