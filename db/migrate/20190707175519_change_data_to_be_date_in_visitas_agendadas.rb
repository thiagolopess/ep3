class ChangeDataToBeDateInVisitasAgendadas < ActiveRecord::Migration[5.2]
  def change
    change_column :visitas_agendadas, :data, :date
  end
end
