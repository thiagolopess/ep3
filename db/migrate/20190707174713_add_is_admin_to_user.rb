class AddIsAdminToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :isAdmin, :boolean, default:0
    add_column :users, :name, :string
    add_column :users, :telefone, :string

  end
end
