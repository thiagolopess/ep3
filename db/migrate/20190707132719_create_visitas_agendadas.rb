class CreateVisitasAgendadas < ActiveRecord::Migration[5.2]
  def change
    create_table :visitas_agendadas do |t|
      t.date :data
      t.string :hora

      t.timestamps
    end
  end
end
