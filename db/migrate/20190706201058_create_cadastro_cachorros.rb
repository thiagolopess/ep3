class CreateCadastroCachorros < ActiveRecord::Migration[5.2]
  def change
    create_table :cadastro_cachorros do |t|
      t.string :nome
      t.string :raca
      t.string :sexo
      t.integer :idade

      t.timestamps
    end
  end
end
