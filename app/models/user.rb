class User < ApplicationRecord
  has_many :visitas_agendadas
  enum status: [ :active, :archived ]

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
