class UserPolicy < ApplicationPolicy
  def index?
    user.isAdmin?
  end
  
  class Scope < Scope
    def resolve
      scope.all
    end
  end

end
