json.extract! visitas_agendada, :id, :data, :hora, :created_at, :updated_at
json.url visitas_agendada_url(visitas_agendada, format: :json)
