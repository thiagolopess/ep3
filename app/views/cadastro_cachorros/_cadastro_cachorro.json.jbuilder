json.extract! cadastro_cachorro, :id, :nome, :raca, :sexo, :idade, :created_at, :updated_at
json.url cadastro_cachorro_url(cadastro_cachorro, format: :json)
