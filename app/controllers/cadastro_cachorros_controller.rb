class CadastroCachorrosController < ApplicationController
  before_action :set_cadastro_cachorro, only: [:show, :edit, :update, :destroy]

  # GET /cadastro_cachorros
  # GET /cadastro_cachorros.json
  def index
    @cadastro_cachorros = CadastroCachorro.all
  end

  # GET /cadastro_cachorros/1
  # GET /cadastro_cachorros/1.json
  def show
  end

  # GET /cadastro_cachorros/new
  def new
    @cadastro_cachorro = CadastroCachorro.new
  end

  # GET /cadastro_cachorros/1/edit
  def edit
  end

  # POST /cadastro_cachorros
  # POST /cadastro_cachorros.json
  def create
    @cadastro_cachorro = CadastroCachorro.new(cadastro_cachorro_params)

    respond_to do |format|
      if @cadastro_cachorro.save
        format.html { redirect_to cadastro_cachorros_path, notice: 'Cadastro cachorro was successfully created.' }
        format.json { render :show, status: :created, location: @cadastro_cachorro }
      else
        format.html { render :new }
        format.json { render json: @cadastro_cachorro.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cadastro_cachorros/1
  # PATCH/PUT /cadastro_cachorros/1.json
  def update
    respond_to do |format|
      if @cadastro_cachorro.update(cadastro_cachorro_params)
        format.html { redirect_to @cadastro_cachorro, notice: 'Cadastro cachorro was successfully updated.' }
        format.json { render :show, status: :ok, location: @cadastro_cachorro }
      else
        format.html { render :edit }
        format.json { render json: @cadastro_cachorro.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cadastro_cachorros/1
  # DELETE /cadastro_cachorros/1.json
  def destroy
    @cadastro_cachorro.destroy
    respond_to do |format|
      format.html { redirect_to cadastro_cachorros_url, notice: 'Cadastro cachorro was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cadastro_cachorro
      @cadastro_cachorro = CadastroCachorro.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cadastro_cachorro_params
      params.require(:cadastro_cachorro).permit(:nome, :raca, :sexo, :idade, :foto)
    end
end
