class ApplicationController < ActionController::Base
  include Pundit

    before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  private
  def user_not_authorized
      flash[:alert] = "Sem permissao"
      redirect_to(request.referrer || root_path)
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit({ roles: [] },:name, :telefone, :email, :password) }
  end
end
